### An example business inventory management desktop application.

Written using the NetBeans Integrated development environment (IDE) version 15.
Java version 18.0.2.

Features
--------
* JavaFX 13. GUI application.
* JavaDoc comments. HTML documentation generator.
* Multi-platform / Operating System (OS). Written and Tested on Debian Linux 12 and Windows 10.
* Create, Read, Update, and Delete (CRUD) parts and products.
* Products depend on parts.
* Part and product search with error messages.
