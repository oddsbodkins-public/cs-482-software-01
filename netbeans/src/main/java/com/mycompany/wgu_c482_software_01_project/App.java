package com.mycompany.wgu_c482_software_01_project;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.GridPane;  
import javafx.scene.control.TextField;
import javafx.scene.text.FontWeight;
import javafx.scene.layout.VBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Border;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.control.Alert;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.stage.Modality;
import java.util.Optional;
import java.util.Arrays;
import javafx.event.Event;

/** * 
 * Class containing JavaFX procedural GUI code, main logic.<br>
 * Attempts to fulfill C482 performance assessment requirements.<br>
 * 
 * Technical details<br><br>
 * 
 * Name: User<br>
 * Date: 12-October-2022<br><br>
 * 
 * NetBeans Project with Mavin build tool<br>
 * JavaFX 13 NetBeans module<br>
 * Product Version: Apache NetBeans IDE 15<br>
 * Java: 18.0.2; OpenJDK 64-Bit Server VM 18.0.2+9<br>
 * Runtime: OpenJDK Runtime Environment 18.0.2+9<br>
 * System: Debian 12, Linux version 5.19.0-1-amd64 running on amd64; UTF-8; en_US (nb)<br><br>
 * 
 * Useful web links / documentation<br><br>
 * 
 * <a href="https://docs.oracle.com/javase/8/javafx/api/allclasses-noframe.html">Oracle JavaFX class documentation</a><br><br>
 * 
 * Oracle JavaFX tutorials: <br><br>
 * 
 * <a href="https://docs.oracle.com/javase/8/javase-clienttechnologies.htm">Oracle JavaFX tutorial #01</a><br>
 * <a href="https://docs.oracle.com/javase/8/javafx/get-started-tutorial/get_start_apps.htm#JFXST804">Oracle JavaFX tutorial #02</a><br>
 * <a href="https://docs.oracle.com/javafx/2/ui_controls/jfxpub-ui_controls.htm">Oracle JavaFX tutorial #03</a><br>
 * 
 * @author User
 */
public class App extends Application {
    private final TableView _partsTableView;
    private final TableView _productsTableView;
    private final static Inventory _inventory = new Inventory();
    
    /**
     *
     */
    public App() {
        // create data for the TableView instance
        _inventory.addPart(new InHouse(1, "Brakes", 15.00, 45, 30, 80, 4001));
        _inventory.addPart(new InHouse(2, "Wheel", 11.00, 25, 10, 100, 4002));
        _inventory.addPart(new InHouse(3, "Seat", 15.00, 55, 35, 250, 4003));
        _inventory.addPart(new InHouse(4, "Rim", 20.00, 15, 5, 75, 4004));        
        
        var product01 = new Product(1, "Unicycle", 100.50, 20, 1, 20);
        var product02 = new Product(2, "Mountain Bike", 300.00, 15, 10, 50);
        product01.addAssociatedPart(_inventory.lookupPart(1));
        product01.addAssociatedPart(_inventory.lookupPart(3));               
        _inventory.addProduct(product01);
        
        product02.addAssociatedPart(_inventory.lookupPart(2));
        product02.addAssociatedPart(_inventory.lookupPart(3));
        _inventory.addProduct(product02);

        this._partsTableView = new TableView<Part> ();
        this._productsTableView = new TableView<Product> ();
    }    
    
    /**
     * Translate the TableView selection to a Part ID
     * @return integer of selected part id
     */
    private int _getPartTableViewSelectionId() {        
        ObservableList<Part> ol =_partsTableView.getSelectionModel().getSelectedItems();
        
        if(ol.size() == 1) { // assume that only one row is selected in the TableView
            Part p = ol.get(0);
            return p.getId();
        }
        
        return -1;
    }

    /**
     * Translate the TableView selection to a Product ID
     * @return integer of selected product id
     */    
    private int _getProductTableViewSelectionId() {        
        ObservableList<Product> ol = _productsTableView.getSelectionModel().getSelectedItems();
        
        if(ol.size() == 1) { // assume that only one row is selected in the TableView
            Product p = ol.get(0);
            return p.getId();
        }
        
        return -1;        
    }
    
    /**
     * Search an ArrayList / ObservableList for a part id, return the index if found or -1.
     * @param partId requested part id
     * @return index in the ObservableList of the requested part id or -1 if not found.
     */
    private int _getInventoryPartIndex(int partId) {
        ObservableList<Part> ol = _inventory.getAllParts();
        for (int i = 0; i < ol.size(); i++) {
            Part p = ol.get(i);
            if(p.getId() == partId) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Search an ArrayList / ObservableList for a product id, return the index if found or -1.
     * @param productId requested product id
     * @return index in the ObservableList of the requested product id or -1 if not found.
     */    
    private int _getInventoryProductIndex(int productId) {
        ObservableList<Product> ol = _inventory.getAllProducts();
        for (int i = 0; i < ol.size(); i++) {
            Product p = ol.get(i);
            if(p.getId() == productId) {
                return i;
            }
        }
        return -1;        
    }

    /**
     * JavaFX GUI helper function. Popup a new window for adding or modifying an existing part.
     * @param partId -1 for a creating a new part or if existing part id (> 0), modifies existing part. [Part ids begin at 1 and increment]
     */
    private void _addOrModifyPartWindow (int partId) {
        Part oldPart = _inventory.lookupPart(partId);
        
        var newWindow = new Stage();
        Label titleLabel;
        newWindow.initModality(Modality.APPLICATION_MODAL);

        if(partId < 0) { // create a new row
            newWindow.setTitle("Add Part Window");
            titleLabel = new Label("Add Part");
        } else {
            newWindow.setTitle("Modify Part Window");
            titleLabel = new Label("Modify Part");
        }

        
        var rootContainer = new VBox();
        rootContainer.setPadding(new Insets(20, 30, 20, 30)); // top, right, bottom, and left
        
        rootContainer.getChildren().add(titleLabel);
        
        // make the label bold
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 18.0));        
                
        var hBoxRadioButtonContainer = new HBox();
        
        // radio buttons
        var partSourceGroup = new ToggleGroup();  
        var inHouseRadioButton = new RadioButton("In-House");  
        var outsourcedRadioButton = new RadioButton("Outsourced");    
        inHouseRadioButton.setToggleGroup(partSourceGroup);
        outsourcedRadioButton.setToggleGroup(partSourceGroup);          
        
        hBoxRadioButtonContainer.getChildren().addAll(inHouseRadioButton, outsourcedRadioButton);
        hBoxRadioButtonContainer.setPadding(new Insets(30, 0, 30, 28)); // top, right, bottom, and left
        hBoxRadioButtonContainer.setSpacing(25.0);
        
        rootContainer.getChildren().add(hBoxRadioButtonContainer);
        
        
        // labels
        Label idLabel, nameLabel, inventoryLabel, priceLabel, maxLabel, minLabel, machineIDLabel, companyNameLabel;
        idLabel = new Label("ID");
        nameLabel = new Label("Name");
        inventoryLabel = new Label("Inventory");
        priceLabel = new Label("Price / Cost          ");
        maxLabel = new Label("Max");
        minLabel = new Label("Min");
        machineIDLabel = new Label("Machine ID");
        companyNameLabel = new Label("Company Name");
        
        // text fields
        TextField idTextField, nameTextField, inventoryTextField, priceTextField, maxTextField, minTextField, machineIDTextField, companyNameTextField;
        idTextField = new TextField();
        nameTextField = new TextField();
        inventoryTextField = new TextField();
        priceTextField = new TextField();
        maxTextField = new TextField();
        minTextField = new TextField();
        machineIDTextField = new TextField();
        companyNameTextField = new TextField();
        
        // organize the labels and text fields with a GridPane
        var gridPaneContainer = new GridPane();
        gridPaneContainer.setHgap(30);
        gridPaneContainer.setVgap(10);
        
        gridPaneContainer.add(idLabel, 1, 0); // column = 1, row = 0
        gridPaneContainer.add(idTextField, 2, 0);

        // disable the Part ID text field
        idTextField.setText("Auto Generated");
        idLabel.setDisable(true);
        idTextField.setDisable(true);
        
        gridPaneContainer.add(nameLabel, 1, 1);
        gridPaneContainer.add(nameTextField, 2, 1);

        gridPaneContainer.add(inventoryLabel, 1, 2);
        gridPaneContainer.add(inventoryTextField, 2, 2);
        
        gridPaneContainer.add(priceLabel, 1, 3);
        gridPaneContainer.add(priceTextField, 2, 3);

        gridPaneContainer.add(maxLabel, 1, 4);
        gridPaneContainer.add(maxTextField, 2, 4);        
        gridPaneContainer.add(minLabel, 3, 4);
        gridPaneContainer.add(minTextField, 4, 4);

        // reserve the space for visual effect, but disable the components        
        machineIDTextField.setDisable(true);
        machineIDLabel.setDisable(true);
        
        gridPaneContainer.add(machineIDLabel, 1, 5);
        gridPaneContainer.add(machineIDTextField, 2, 5);
        
        // the last Label and TextField elements depend on the RadioButton selection
        
        // radio button event listeners
        inHouseRadioButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                gridPaneContainer.getChildren().remove(companyNameLabel);
                gridPaneContainer.getChildren().remove(companyNameTextField);                

                gridPaneContainer.getChildren().remove(machineIDLabel);
                gridPaneContainer.getChildren().remove(machineIDTextField);
                
                machineIDTextField.setDisable(false);
                machineIDLabel.setDisable(false);
                
                gridPaneContainer.add(machineIDLabel, 1, 5);
                gridPaneContainer.add(machineIDTextField, 2, 5);
            }
        });
        
        outsourcedRadioButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                gridPaneContainer.getChildren().remove(machineIDLabel);
                gridPaneContainer.getChildren().remove(machineIDTextField);
                
                gridPaneContainer.add(companyNameLabel, 1, 5);
                gridPaneContainer.add(companyNameTextField, 2, 5);            
            }
        });
        
        if(partId > 0) { // update an existing row, fire ActionEvent for RadioButton to show the correct TextField and Label
            if(oldPart instanceof InHouse) {
                Event.fireEvent(inHouseRadioButton, new ActionEvent());
            } else if (oldPart instanceof Outsourced) {
                Event.fireEvent(outsourcedRadioButton, new ActionEvent());
            }
        }
        
        rootContainer.getChildren().add(gridPaneContainer);
       

        // buttons
        Button saveButton, cancelButton;
        saveButton = new Button("Save");
        cancelButton = new Button("Cancel");

        // update an existing row
        // populate the text fields with previously saved data
        if (partId > 0) {
            if(oldPart instanceof InHouse) {
                inHouseRadioButton.setSelected(true);                
            } else if (oldPart instanceof Outsourced) {
                outsourcedRadioButton.setSelected(true);
            }

            idTextField.setText(String.valueOf(oldPart.getId()));
            nameTextField.setText(oldPart.getName());
            inventoryTextField.setText(String.valueOf(oldPart.getStock()));
            priceTextField.setText(String.valueOf(oldPart.getPrice()));
            maxTextField.setText(String.valueOf(oldPart.getMax()));
            minTextField.setText(String.valueOf(oldPart.getMin()));
            
            if (oldPart instanceof InHouse) {
                machineIDTextField.setText( String.valueOf(((InHouse) oldPart).getMachineId()));                
            } else if (oldPart instanceof Outsourced) {
                companyNameTextField.setText( ((Outsourced) oldPart).getCompanyName() );
            }
        }
        
        // 'Save' button pressed
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                if (partId > 0) { // update an existing row
                    // temporary holding variables
                    int id, stock, min, max, machineId;
                    String name, companyName;
                    double price;
                    
                    id = stock = min = max = machineId = 0;
                    name = companyName = "";                  
                    price = 0.0;
                    
                    // part id is already set, do not change it
                    id = partId;
                    name = nameTextField.getText();

                    // validate form elements data types
                    if (Utilities.isValidPositiveDouble(priceTextField.getText())) {
                        price = Double.parseDouble(priceTextField.getText());
                    } else {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid price value! (Ex: 9.75)");
                        invalidElementAlert.show();
                        return;
                    }
                    
                    if (Utilities.isValidPositiveInteger(maxTextField.getText())) {
                        max = Integer.parseInt(maxTextField.getText());
                    } else {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid max value! (Ex: 50)");
                        invalidElementAlert.show();
                        return;                        
                    }

                    if (Utilities.isValidPositiveInteger(minTextField.getText())) {
                        min = Integer.parseInt(minTextField.getText());
                    } else {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid min value! (Ex: 15)");
                        invalidElementAlert.show();
                        return;
                    }
                    
                    if (Utilities.isValidPositiveInteger(inventoryTextField.getText())) {
                        stock = Integer.parseInt(inventoryTextField.getText());
                    } else {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid stock / inventory value!");
                        invalidElementAlert.show();
                        return;
                    }

                    // verify that min, max, and inventory are within proper ranges
                    if(min > max) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Min value must be less than max value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(max < min) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Max value must be greater than min value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(!(stock <= max && stock >= min)) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Stock / inventory value must be between min and max!");
                        invalidElementAlert.show();
                        return;
                    }    
                    

                    
                    if(inHouseRadioButton.isSelected()) {
                        // validate the machine id field data type
                        if (!Utilities.isValidPositiveInteger(machineIDTextField.getText())) {
                            var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid machine id value! (Ex: 4005)");
                            invalidElementAlert.show();
                            return;
                        }           
                        
                        if(oldPart instanceof InHouse) { // the radio button value is the same as before the modification
                            machineId = Integer.parseInt(machineIDTextField.getText());
                            InHouse newPart = new InHouse(id, name, price, stock, min, max, machineId);
                            _inventory.updatePart(_getInventoryPartIndex(partId), newPart);
                            _partsTableView.setItems(_inventory.getAllParts());
                        } else { // the radio button value is different than before, we need a new child class of Part (was Outsourced instance)
                            machineId = Integer.parseInt(machineIDTextField.getText());
                            InHouse newPart = new InHouse(id, name, price, stock, min, max, machineId);
                            
                            _inventory.updatePart(_getInventoryPartIndex(partId), newPart);
                            _partsTableView.setItems(_inventory.getAllParts());
                        }

                    } else if (outsourcedRadioButton.isSelected()) {
                        
                        if(oldPart instanceof Outsourced) { // the radio button value is the same as before the modification
                            companyName = companyNameTextField.getText();
                            Outsourced newPart = new Outsourced(id, name, price, stock, min, max, companyName);
                            _inventory.updatePart(_getInventoryPartIndex(partId), newPart);
                            _partsTableView.setItems(_inventory.getAllParts());
                        } else { // the radio button value is different than before, we need a new child class of Part (was InHouse instance)
                            companyName = companyNameTextField.getText();
                            Outsourced newPart = new Outsourced(id, name, price, stock, min, max, companyName);
                            _inventory.updatePart(_getInventoryPartIndex(partId), newPart);
                            _partsTableView.setItems(_inventory.getAllParts());
                        }                 
                    }                
                } else { // create a new row
                    
                    // find the highest known Part id, increment it
                    int highestId = -1;
                    ObservableList<Part> ol = _inventory.getAllParts();
                    for (int i = 0; i < ol.size(); i++) {
                        if(ol.get(i).getId() > highestId) {
                            highestId = ol.get(i).getId();                            
                        }
                    }
                    highestId += 1;


                    // validate form elements data types
                    if (!Utilities.isValidPositiveDouble(priceTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid price value! (Ex: 9.75)");
                        invalidElementAlert.show();
                        return;
                    }                    
                    if (!Utilities.isValidPositiveInteger(maxTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid max value! (Ex: 50)");
                        invalidElementAlert.show();
                        return;                        
                    }
                    if (!Utilities.isValidPositiveInteger(minTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid min value! (Ex: 15)");
                        invalidElementAlert.show();
                        return;
                    }                    
                    if (!Utilities.isValidPositiveInteger(inventoryTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid stock / inventory value!");
                        invalidElementAlert.show();
                        return;
                    }                    
                    
                    
                    String name = nameTextField.getText();
                    double price = Double.parseDouble(priceTextField.getText());
                    int stock = Integer.parseInt(inventoryTextField.getText());
                    int min = Integer.parseInt(minTextField.getText());
                    int max = Integer.parseInt(maxTextField.getText());                    

                    // verify that min, max, and inventory are within proper ranges
                    if(min > max) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Min value must be less than max value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(max < min) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Max value must be greater than min value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(!(stock <= max && stock >= min)) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Stock / inventory value must be between min and max!");
                        invalidElementAlert.show();
                        return;
                    }
                    
                    if (inHouseRadioButton.isSelected()) {
                        // validate the machine id field data type
                        if (!Utilities.isValidPositiveInteger(machineIDTextField.getText())) {
                            var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid machine id value! (Ex: 4005)");
                            invalidElementAlert.show();
                            return;
                        }                        
                        
                        int machineId = Integer.parseInt(machineIDTextField.getText());
                        InHouse newPart = new InHouse(highestId, name, price, stock, min, max, machineId);
                        _inventory.addPart(newPart);
                        _partsTableView.setItems(_inventory.getAllParts());
                    } else if (outsourcedRadioButton.isSelected()) {
                        String companyName = companyNameTextField.getText();
                        Outsourced newPart = new Outsourced(highestId, name, price, stock, min, max, companyName);
                        _inventory.addPart(newPart);
                        _partsTableView.setItems(_inventory.getAllParts());
                    }                    
                }
                                
                newWindow.close();
            }
        });

        // 'Cancel' button pressed
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                newWindow.close();
            }
        });
        
        // organize buttons with hbox container
        var buttonsHBoxContainer = new HBox();
        buttonsHBoxContainer.setPadding(new Insets(30, 0, 40, 30)); // top, right, bottom, and left
        buttonsHBoxContainer.setSpacing(10.0);
        buttonsHBoxContainer.getChildren().addAll(saveButton, cancelButton);        
        
        rootContainer.getChildren().add(buttonsHBoxContainer);

        newWindow.setScene(new Scene(rootContainer));
        newWindow.show();
    }

    /**
     * JavaFX GUI helper function. Popup a new window for adding or modifying an existing product.
     * @param partId -1 for a creating a new product or if existing product id (> 0), modifies existing product. [Product ids begin at 1 and increment]
     */    
    private void _addOrModifyProductWindow(int productId) {
        Product oldProduct = _inventory.lookupProduct(productId);
        ObservableList<Part> associatedParts;

        var allPartsTableView = new TableView();
        var partsUsedByCurrentProductTableView = new TableView();        
        
        var newWindow = new Stage();
        Label titleLabel;
        newWindow.initModality(Modality.APPLICATION_MODAL);
        
        
        if(productId < 0) { // create a new row
            newWindow.setTitle("Add Product Window");
            titleLabel = new Label("Add Product");
            associatedParts = FXCollections.observableArrayList();
        } else {
            newWindow.setTitle("Modify Product Window");
            titleLabel = new Label("Modify Product");
            associatedParts = oldProduct.getAllAssociatedParts();
        }
        
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));        
        
        var rootContainer = new VBox();
        rootContainer.setPadding(new Insets(20, 30, 20, 30)); // top, right, bottom, and left
        rootContainer.getChildren().add(titleLabel);
        
        var bodyContainer = new HBox();
        bodyContainer.setPadding(new Insets(0)); // top, right, bottom, and left
        
        // Left side of the window (product form fields and labels)
        // organize the labels and text fields with a GridPane
        var leftSideContainer = new GridPane();
        leftSideContainer.setPadding(new Insets(40, 20, 0, 0)); // top, right, bottom, and left
        leftSideContainer.setHgap(10);
        leftSideContainer.setVgap(20);        

        var productIdLabel = new Label("ID");
        var productNameLabel = new Label("Name");
        var productInventoryLabel = new Label("Inventory");
        var productPriceLabel = new Label("Price");
        var productMaxLabel = new Label("Max");
        var productMinLabel = new Label("Min");
        
        var productIdTextField = new TextField();
        productIdTextField.setDisable(true);
        productIdTextField.setText("Auto Generated");
        productIdTextField.setPrefWidth(150.0);
        var productNameTextField = new TextField();
        productNameTextField.setPrefWidth(150.0);
        var productInventoryTextField = new TextField();
        productInventoryTextField.setPrefWidth(150.0);
        var productPriceTextField = new TextField();
        productPriceTextField.setPrefWidth(150.0);
        var productMaxTextField = new TextField();
        productMaxTextField.setPrefWidth(150.0);
        var productMinTextField = new TextField();
        productMinTextField.setPrefWidth(150.0);
        
        // populate form fields with existing product data
        if(productId > 0) { // modify existing product
            productIdTextField.setText(String.valueOf(oldProduct.getId()));
            productNameTextField.setText(oldProduct.getName());
            productInventoryTextField.setText(String.valueOf(oldProduct.getStock()));
            productPriceTextField.setText(String.valueOf(oldProduct.getPrice()));
            productMaxTextField.setText(String.valueOf(oldProduct.getMax()));
            productMinTextField.setText(String.valueOf(oldProduct.getMin()));
        }
        
        // row 1        
        leftSideContainer.add(productIdLabel, 1, 1); // column = 1, row = 0
        leftSideContainer.add(productIdTextField, 2, 1);
                
        // row 2
        leftSideContainer.add(productNameLabel, 1, 2);
        leftSideContainer.add(productNameTextField, 2, 2);
        
        // row 3
        leftSideContainer.add(productInventoryLabel, 1, 3);
        leftSideContainer.add(productInventoryTextField, 2, 3);
        
        // row 4
        leftSideContainer.add(productPriceLabel, 1, 4);
        leftSideContainer.add(productPriceTextField, 2, 4);

        // row 5
        leftSideContainer.add(productMaxLabel, 1, 5);
        leftSideContainer.add(productMaxTextField, 2, 5);
        leftSideContainer.add(productMinLabel, 3, 5);
        leftSideContainer.add(productMinTextField, 4, 5);

        bodyContainer.getChildren().add(leftSideContainer);
        
        
        // right side of the window (TableViews, buttons, etc)
        var rightSideContainer = new VBox();
        rightSideContainer.setPadding(new Insets(0, 0, 10, 0)); // top, right, bottom, and left        

        var searchContainer = this._getPartSearchSection(allPartsTableView);
        rightSideContainer.getChildren().add(searchContainer);
        
        var allPartsTableLabel = new Label("All available parts");
        allPartsTableLabel.setAlignment(Pos.TOP_LEFT);
        allPartsTableLabel.setPadding(new Insets(0, 0, 10, 0)); // top, right, bottom, and left
        rightSideContainer.getChildren().add(allPartsTableLabel);
        
        allPartsTableView.setPrefSize(500, 200); // width, height
        allPartsTableView.setEditable(false);
        rightSideContainer.getChildren().add(allPartsTableView);

        // add the column titles
        var partIDTableColumn = new TableColumn("Part ID");
        partIDTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        partIDTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("id")
        );
        
        var partNameTableColumn = new TableColumn("Part Name");
        partNameTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 25); // set percentage of width
        partNameTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("name")
        );
        
        var inventoryLevelTableColumn = new TableColumn("Inventory Level");
        inventoryLevelTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 25); // set percentage of width
        inventoryLevelTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("stock")
        );
        
        var priceTableColumn = new TableColumn("Price / Cost per Unit");
        priceTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 35); // set percentage of width
        priceTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("price")
        );
        
        allPartsTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        allPartsTableView.getColumns().addAll(partIDTableColumn, partNameTableColumn, inventoryLevelTableColumn, priceTableColumn);
        allPartsTableView.setItems(_inventory.getAllParts());
                       

        var addPartButtonContainer = new VBox();
        addPartButtonContainer.setPadding(new Insets(15, 0, 15, 0)); // top, right, bottom, and left
        var addPartButton = new Button("Add");
        addPartButtonContainer.setAlignment(Pos.TOP_RIGHT);
        addPartButtonContainer.getChildren().add(addPartButton);
        rightSideContainer.getChildren().add(addPartButtonContainer);
        
        var partsUsedByCurrentProductTableLabel = new Label("Parts associated with current product");
        partsUsedByCurrentProductTableLabel.setAlignment(Pos.TOP_LEFT);
        partsUsedByCurrentProductTableLabel.setPadding(new Insets(0, 0, 10, 0)); // top, right, bottom, and left
        rightSideContainer.getChildren().add(partsUsedByCurrentProductTableLabel);
        
        partsUsedByCurrentProductTableView.setPrefSize(500, 200);
        partsUsedByCurrentProductTableView.setEditable(false);
        rightSideContainer.getChildren().add(partsUsedByCurrentProductTableView);

        // add the column titles

        var partIDTableColumn2 = new TableColumn("Part ID");
        partIDTableColumn2.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        partIDTableColumn2.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("id")
        );
        
        var partNameTableColumn2 = new TableColumn("Part Name");
        partNameTableColumn2.setMaxWidth(1f * Integer.MAX_VALUE * 25); // set percentage of width
        partNameTableColumn2.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("name")
        );
        
        var inventoryLevelTableColumn2 = new TableColumn("Inventory Level");
        inventoryLevelTableColumn2.setMaxWidth(1f * Integer.MAX_VALUE * 25); // set percentage of width
        inventoryLevelTableColumn2.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("stock")
        );
        
        var priceTableColumn2 = new TableColumn("Price / Cost per Unit");
        priceTableColumn2.setMaxWidth(1f * Integer.MAX_VALUE * 35); // set percentage of width
        priceTableColumn2.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("price")
        );        

        partsUsedByCurrentProductTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        partsUsedByCurrentProductTableView.getColumns().addAll(partIDTableColumn2, partNameTableColumn2, inventoryLevelTableColumn2, priceTableColumn2);
        
        // populate the associated parts table with existing product data
        if (productId > 0) {
            partsUsedByCurrentProductTableView.setItems(associatedParts);   
        }
        
        
        var removePartButtonContainer = new VBox();
        removePartButtonContainer.setPadding(new Insets(15, 0, 10, 0)); // top, right, bottom, and left
        var removePartButton = new Button("Remove Associated Part");
        removePartButtonContainer.setAlignment(Pos.TOP_RIGHT);
        removePartButtonContainer.getChildren().add(removePartButton);
        rightSideContainer.getChildren().add(removePartButtonContainer);
        
        bodyContainer.getChildren().add(rightSideContainer);        
        rootContainer.getChildren().add(bodyContainer);

        // 'Add Part' button pressed
        addPartButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {                
                if(allPartsTableView.getSelectionModel().isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "You must select a table row!");
                    alert.show();
                } else {
                    ObservableList<Part> ol = allPartsTableView.getSelectionModel().getSelectedItems();

                    if(ol.size() == 1) { // assume that only one row is selected in the TableView
                        Part newPart = ol.get(0);

                        boolean partAlreadyExists = false;
                        
                        // loop through existing associated parts
                        for (int i = 0; i < associatedParts.size(); i++) {
                            if (associatedParts.get(i).getId() == newPart.getId()) {
                                partAlreadyExists = true;
                            }
                        }
                        
                        // do not add existing parts
                        if (partAlreadyExists == false) {
                            associatedParts.add(newPart);
                        } else {
                            Alert alert = new Alert(Alert.AlertType.ERROR, "That part has already been associated with this product!");
                            alert.show();                            
                        }

                        partsUsedByCurrentProductTableView.setItems(associatedParts);
                    }

                }
            }
        });
        
        // 'Remove Associated Part' button pressed
        removePartButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {                
                if(partsUsedByCurrentProductTableView.getSelectionModel().isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "You must select a table row!");
                    alert.show();                    
                } else {
                    ObservableList<Part> ol = partsUsedByCurrentProductTableView.getSelectionModel().getSelectedItems();
                                        
                    if(ol.size() == 1) { // assume that only one row is selected in the TableView
                        associatedParts.remove(ol.get(0));
                    }
                      
                }
            }
        });
        
        // final buttons row go inside rootContainer (VBox)
        
        var bottomButtonsContainer = new HBox();
        bottomButtonsContainer.setSpacing(10.0);
        var saveButton = new Button("Save");
        var cancelButton = new Button("Cancel");
        bottomButtonsContainer.getChildren().addAll(saveButton, cancelButton);
        bottomButtonsContainer.setAlignment(Pos.TOP_RIGHT);
        rootContainer.getChildren().add(bottomButtonsContainer);
        
        // 'Save' button pressed
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                if(productId < 1) { // create a new product

                    // validate form elements data types
                    if (!Utilities.isValidPositiveDouble(productPriceTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid price value! (Ex: 9.75)");
                        invalidElementAlert.show();
                        return;
                    }                    
                    if (!Utilities.isValidPositiveInteger(productMaxTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid max value! (Ex: 50)");
                        invalidElementAlert.show();
                        return;                        
                    }
                    if (!Utilities.isValidPositiveInteger(productMinTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid min value! (Ex: 15)");
                        invalidElementAlert.show();
                        return;
                    }                    
                    if (!Utilities.isValidPositiveInteger(productInventoryTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid stock / inventory value!");
                        invalidElementAlert.show();
                        return;
                    }              
                    
                    
                    int newId = 0;
                    String newName = productNameTextField.getText();
                    double newPrice = Double.parseDouble(productPriceTextField.getText());
                    int newInventory = Integer.parseInt(productInventoryTextField.getText());
                    int newMin = Integer.parseInt(productMinTextField.getText());
                    int newMax = Integer.parseInt(productMaxTextField.getText());

                    // verify that min, max, and inventory are within proper ranges
                    if(newMin > newMax) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Min value must be less than max value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(newMax < newMin) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Max value must be greater than min value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(!(newInventory <= newMax && newInventory >= newMin)) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Stock / inventory value must be between min and max!");
                        invalidElementAlert.show();
                        return;
                    }
                    
                    ObservableList<Product> ol = _inventory.getAllProducts();
                    
                    // search for the highest existing id, and increment it
                    for(int i = 0; i < ol.size(); i++) {
                        if(ol.get(i).getId() > newId) {
                            newId = ol.get(i).getId();
                        }
                    }
                    newId += 1;
                    
                    var newProduct = new Product(newId, newName, newPrice, newInventory, newMin, newMax);
                    
                    // add all of the associated Part objects
                    for (int i = 0; i < associatedParts.size(); i++) {
                        newProduct.addAssociatedPart(associatedParts.get(i));
                    }
                                        
                    _inventory.addProduct(newProduct);
                    
                    // update the table on the main form
                    _productsTableView.setItems(_inventory.getAllProducts());
                    newWindow.close();                    
                } else { // update an existing product row
                    
                    // validate form elements data types
                    if (!Utilities.isValidPositiveDouble(productPriceTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid price value! (Ex: 9.75)");
                        invalidElementAlert.show();
                        return;
                    }                    
                    if (!Utilities.isValidPositiveInteger(productMaxTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid max value! (Ex: 50)");
                        invalidElementAlert.show();
                        return;                        
                    }
                    if (!Utilities.isValidPositiveInteger(productMinTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid min value! (Ex: 15)");
                        invalidElementAlert.show();
                        return;
                    }                    
                    if (!Utilities.isValidPositiveInteger(productInventoryTextField.getText())) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Invalid stock / inventory value!");
                        invalidElementAlert.show();
                        return;
                    }

                    String newName = productNameTextField.getText();
                    double newPrice = Double.parseDouble(productPriceTextField.getText());
                    int newInventory = Integer.parseInt(productInventoryTextField.getText());
                    int newMin = Integer.parseInt(productMinTextField.getText());
                    int newMax = Integer.parseInt(productMaxTextField.getText());

                    // verify that min, max, and inventory are within proper ranges
                    if(newMin > newMax) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Min value must be less than max value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(newMax < newMin) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Max value must be greater than min value!");
                        invalidElementAlert.show();
                        return;
                    }
                    if(!(newInventory <= newMax && newInventory >= newMin)) {
                        var invalidElementAlert = new Alert(Alert.AlertType.ERROR, "Stock / inventory value must be between min and max!");
                        invalidElementAlert.show();
                        return;
                    }
                    
                    Product newProduct = new Product(productId, newName, newPrice, newInventory, newMin, newMax);                    
                                        
                    // add all of the associated Part objects
                    for (int i = 0; i < associatedParts.size(); i++) {
                        newProduct.addAssociatedPart(associatedParts.get(i));
                    }
                    
                    _inventory.updateProduct(_getInventoryProductIndex(productId), newProduct);
                    
                    // update the table on the main form
                    _productsTableView.setItems(_inventory.getAllProducts());
                    newWindow.close();
                }
            }
        });
        
        // 'Cancel' button pressed
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                newWindow.close();
            }
        });
        
        newWindow.setScene(new Scene(rootContainer));
        newWindow.show();
    }
    
    /**
     * Event handler for Main Form Window Top Menu.
     * This function handles modifying an existing part.
     * @return JavaFX EventHandler, this is attached to the MenuItem type.
     */
    private EventHandler<ActionEvent> _getModifyPartTopMenuEventHandler() {        
        return new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                ObservableList<Part> ol = _inventory.getAllParts();
                String []partRowStrings = new String[ol.size()];     

                // Create an array of strings with the existing parts
                for (int i = 0; i < ol.size(); i++) {
                    partRowStrings[i] = "Name: '" + ol.get(i).getName() + "' (ID: " + ol.get(i).getId() + ")";
                }
                
                
                var newWindow = new Stage();
                newWindow.initModality(Modality.APPLICATION_MODAL);
                newWindow.setTitle("Modify an existing part");
                var titleLabel = new Label("Part:");

                
                var rootContainer = new VBox();
                rootContainer.setPadding(new Insets (30, 50, 30, 50)); // top, right, bottom, left
                var dropDownContainer = new HBox();
                dropDownContainer.setSpacing(30.0);
                
                MenuButton m = new MenuButton("Select a part");
                                   
                // create an event handler for each MenuItem
                EventHandler<ActionEvent> menuItemEventHandler = new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent e) {
                        int index = Arrays.asList(partRowStrings).indexOf(((MenuItem)e.getSource()).getText());
                        if(index > -1) {
                            newWindow.close();
                            Part p = ol.get(index);                            
                            _addOrModifyPartWindow(p.getId());
                        }
                        
                    }                    
                };               
                
                // add all the existing parts to the ContextMenu
                for (int i = 0; i < partRowStrings.length; i++) {
                    var mi = new MenuItem(partRowStrings[i]);
                    mi.setOnAction(menuItemEventHandler);
                    m.getItems().add(mi);
                }    
                
                dropDownContainer.getChildren().addAll(titleLabel, m);
                rootContainer.getChildren().add(dropDownContainer);
                
                newWindow.setScene(new Scene(rootContainer));
                newWindow.show();
            }
        };
    }

    /**
     * Event handler for Main Form Window Top Menu.
     * This function handles deleting an existing part.
     * @return JavaFX EventHandler, this is attached to the MenuItem type.
     */    
    private EventHandler<ActionEvent> _getDeletePartTopMenuEventHandler() {
        return new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                ObservableList<Part> ol = _inventory.getAllParts();
                String []partRowStrings = new String[ol.size()];     

                // Create an array of strings with the existing parts
                for (int i = 0; i < ol.size(); i++) {
                    partRowStrings[i] = "Name: '" + ol.get(i).getName() + "' (ID: " + ol.get(i).getId() + ")";
                }
                
                
                var newWindow = new Stage();
                newWindow.initModality(Modality.APPLICATION_MODAL);
                newWindow.setTitle("Delete an existing part");
                var titleLabel = new Label("Part:");

                
                var rootContainer = new VBox();
                rootContainer.setPadding(new Insets (30, 50, 30, 50)); // top, right, bottom, left
                var dropDownContainer = new HBox();
                dropDownContainer.setSpacing(30.0);
                
                MenuButton m = new MenuButton("Select a part");
                
                // create an event handler for each MenuItem
                EventHandler<ActionEvent> menuItemEventHandler = new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent e) {
                        String s = ((MenuItem)e.getSource()).getText();
                        s = s.substring(6, s.length()); // remove part of the string, it helps with searching for a character ':'
                        int partId = Integer.parseInt(s.substring(s.indexOf(":") + 2, s.length() - 1));
                                                
                        if(partId > -1) {                            
                            // show a confirmation dialog window
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure that you want to delete that row?");

                            var noButton = new ButtonType("No");
                            var yesButton = new ButtonType("Yes");

                            alert.getButtonTypes().setAll(noButton, yesButton);

                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == yesButton) {
                                Part p = _inventory.lookupPart(partId);
                                _inventory.deletePart(p);
                                _partsTableView.setItems(_inventory.getAllParts());
                            } else if (result.get() == noButton) {
                                // do nothing
                            }                 
                        }
                        newWindow.close();                        
                    }                    
                };               
                
                // add all the existing parts to the ContextMenu
                for (int i = 0; i < partRowStrings.length; i++) {
                    var mi = new MenuItem(partRowStrings[i]);
                    mi.setOnAction(menuItemEventHandler);
                    m.getItems().add(mi);
                }    
                
                dropDownContainer.getChildren().addAll(titleLabel, m);
                rootContainer.getChildren().add(dropDownContainer);
                
                newWindow.setScene(new Scene(rootContainer));
                newWindow.show();                
            }
        };
    }

    /**
     * JavaFX GUI organization function.
     * Creates a window top menu for the main form element.
     * 
     * FUTURE ENHANCEMENT
     * 
     * Add a JavaFX Menu and MenuItems for adding, modifying, and deleting product units.
     * There is an existing 'Parts' top menu. Add a similar element for the 'Product' changes.
     * Position it to the right of the 'Parts' top menu.
     * 
     * @return JavaFX MenuBar, the class representing the traditional 'File, Edit, ...' menu attached to a window container.
     */    
    private MenuBar _getMainFormTopMenu() {        
        final var fileMenu = new Menu("File");
        final var fileMenuExitItem = new MenuItem("Exit");
        
        // menu item event listener
        fileMenuExitItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                System.exit (0);
            }
        });
                
        fileMenu.getItems().add(fileMenuExitItem);
        
        final var helpMenu = new Menu("Help");
        final var helpMenuAboutItem = new MenuItem("About");

        // menu item event listener        
        helpMenuAboutItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Base64.Decoder decoder = Base64.getDecoder();                
                String s = "VHdvIHN0dWRlbnRzIGFyZSBsZWF2aW5nIGEgSmF2YSBDb21wdXRlciBTY2llbmNlIENsYXNzLgpUaGUgbWFuIHR1cm5zIHRvIHRoZSB3b21hbiBhbmQgYXNrcyAiU28uLi4gaG93IG11Y2ggZG8geW91IHdlaWdoPyIKVGhlIHdvbWFuIHNheXMsICJJJ20gbm90IHRlbGxpbmcgeW91ISBUaGF0J3MgcHJpdmF0ZSEiClN1cnByaXNlZCwgdGhlIG1hbiBzYXlzICJCdXQgSSB0aG91Z2h0IHdlIHdlcmUgaW4gdGhlIHNhbWUgY2xhc3MhISI=";                
                String s2 = new String(decoder.decode(s));                
                
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Class: WGU C482 - Software 1\nAssignment: Performance Assessment\n\n-------------------------------\n\n" + s2);                
                alert.setWidth(500);
                alert.setTitle("About this program");
                alert.setHeaderText("Information");
                
                alert.show();
            }
        });
        
        helpMenu.getItems().add(helpMenuAboutItem);
        
        final var partsMenu = new Menu("Parts");
        final var partsMenuAddItem = new MenuItem("Add a part");
        final var partsMenuModifyItem = new MenuItem("Modify a part");
        final var partsMenuDeleteItem = new MenuItem("Delete a part");
        
        partsMenuAddItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                _addOrModifyPartWindow(-1);
            }
        });

        partsMenuModifyItem.setOnAction(this._getModifyPartTopMenuEventHandler());        
        partsMenuDeleteItem.setOnAction(this._getDeletePartTopMenuEventHandler());
        
        partsMenu.getItems().addAll(partsMenuAddItem, partsMenuModifyItem, partsMenuDeleteItem);
        
        var menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, partsMenu,helpMenu);
        
        return menuBar;
    }
    
    /**
     * JavaFX GUI organization helper function. Creates the left side of the main form window, which is the parts TableView and container.
     * @return JavaFX VBox, a vertically organized element container
     */
    private VBox _getMainFormPartsPane() {
        var rootContainer = new VBox();
        var border = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(10), BorderWidths.DEFAULT, new Insets(10)));
        rootContainer.setPrefWidth(600);
        rootContainer.setBorder(border); // add the black rounded corner border
        rootContainer.setPadding(new Insets (10)); // top, right, bottom, left
        
        // first section of VBox
        var titleLabel = new Label("Parts Table");
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        rootContainer.getChildren().add(titleLabel);

        
        // second section of VBox
        var partsSearchContainer = this._getPartSearchSection(this._partsTableView);
        rootContainer.getChildren().add(partsSearchContainer);

        // third section of VBox
        var partsTableSection = this._getPartsTableContainer();
        rootContainer.getChildren().add(partsTableSection);      
       
        // fourth section of VBox
        var partsButtonContainer = new HBox();
        partsButtonContainer.setAlignment(Pos.TOP_RIGHT);
        partsButtonContainer.setPadding(new Insets (10, 0, 10, 0)); // top, right, bottom, left
        
        var addButton = new Button("Add");
        var modifyButton = new Button("Modify");
        var deleteButton = new Button("Delete");
        HBox.setMargin (addButton, new Insets (0, 5, 0, 5)); // top, right, bottom, left
        HBox.setMargin (modifyButton, new Insets (0, 5, 0, 5));
        HBox.setMargin (deleteButton, new Insets (0, 0, 0, 5));
        partsButtonContainer.getChildren().addAll(addButton, modifyButton, deleteButton);
        
        /*
            Button event handlers
        */
        
        // 'Add' button pressed
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                _addOrModifyPartWindow(-1);
            }
        });

        // 'Modify' button pressed
        modifyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {    
                if(_partsTableView.getSelectionModel().isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "You must select a table row!");
                    alert.show();                  
                } else {
                    _addOrModifyPartWindow(_getPartTableViewSelectionId());
                }
            }
        });

        // 'Delete' button pressed
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                if(_partsTableView.getSelectionModel().isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "You must select a table row!");
                    alert.show();    
                } else {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure that you want to delete that row?");
                                        
                    var noButton = new ButtonType("No");
                    var yesButton = new ButtonType("Yes");
                    
                    alert.getButtonTypes().setAll(noButton, yesButton);
                    
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == yesButton) {
                        Part p = _inventory.lookupPart(_getPartTableViewSelectionId());
                        _inventory.deletePart(p);
                        _partsTableView.setItems(_inventory.getAllParts());
                    } else if (result.get() == noButton) {
                        // do nothing
                    }
                }
            }
        });        
        
        
        rootContainer.getChildren().add(partsButtonContainer);       
        
        return rootContainer;
    }

    /**
     * JavaFX GUI organization helper function. Creates the right side of the main form window, which is the product TableView and container.
     * @return JavaFX VBox, a vertically organized element container
     */    
    private VBox _getMainFormProductsPane() {
        var rootContainer = new VBox();
        var border = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(10), BorderWidths.DEFAULT, new Insets(10)));

        rootContainer.setBorder(border);
        rootContainer.setPrefWidth(600);
        rootContainer.setPadding(new Insets (10)); // top, right, bottom, left
        
        // first section of VBox
        var titleLabel = new Label("Product Table");
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        rootContainer.getChildren().add(titleLabel);
        
        // second section of VBox
        var productSearchContainer = this._getProductsSearchSection(this._productsTableView);
        rootContainer.getChildren().add(productSearchContainer);

        // third section of VBox
        var productTableSection = this._getProductsTableContainer();
        rootContainer.getChildren().add(productTableSection);        

        // fourth section of VBox
        var productButtonContainer = new HBox();
        productButtonContainer.setAlignment(Pos.TOP_RIGHT);
        productButtonContainer.setPadding(new Insets (10, 0, 10, 0)); // top, right, bottom, left
        
        var addButton = new Button("Add");
        var modifyButton = new Button("Modify");
        var deleteButton = new Button("Delete");
        HBox.setMargin (addButton, new Insets (0, 5, 0, 5)); // top, right, bottom, left
        HBox.setMargin (modifyButton, new Insets (0, 5, 0, 5));
        HBox.setMargin (deleteButton, new Insets (0, 0, 0, 5));
        productButtonContainer.getChildren().addAll(addButton, modifyButton, deleteButton);
        rootContainer.getChildren().add(productButtonContainer);

        /*
            Button event handlers
        */
        
        // 'Add' button pressed
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                _addOrModifyProductWindow(-1);
            }
        });

        // 'Modify' button pressed
        modifyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {    
                if(_productsTableView.getSelectionModel().isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "You must select a table row!");
                    alert.show();             
                } else {
                    _addOrModifyProductWindow(_getProductTableViewSelectionId());
                }
            }
        });

        // 'Delete' button pressed
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {    
                if(_productsTableView.getSelectionModel().isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "You must select a table row!");
                    alert.show();                  
                } else {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure that you want to delete that row?");
                                        
                    var noButton = new ButtonType("No");
                    var yesButton = new ButtonType("Yes");
                    
                    alert.getButtonTypes().setAll(noButton, yesButton);
                    
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == yesButton) {
                        Product p = _inventory.lookupProduct(_getProductTableViewSelectionId());
                        if(p.getAllAssociatedParts().size() > 0) {
                            Alert alert2 = new Alert(Alert.AlertType.ERROR, "You cannot delete a product with associated parts!");
                            alert2.show();                            
                        } else {
                            _inventory.deleteProduct(p);
                            _productsTableView.setItems(_inventory.getAllProducts());
                        }

                    } else if (result.get() == noButton) {
                        // do nothing
                    }
                }
            }
        });
        
        
        return rootContainer;
    }    

    /**
     * JavaFX GUI helper function for searching the parts table
     * 
     * @param tableToUpdate TableView, table element to update rows when user inputs text to search
     * @return HBox, a horizontally organized container with GUI elements
     */
    private HBox _getPartSearchSection(TableView tableToUpdate) {
        var rootContainer = new HBox();
        rootContainer.setSpacing(10.0);
        rootContainer.setAlignment(Pos.TOP_RIGHT);

        var partsSearchLabel = new Label("Search");
        var partsSearchTextField = new TextField ();
        
        partsSearchTextField.setPromptText("Part ID or Name");
        partsSearchTextField.setPrefWidth(200);
        rootContainer.getChildren().addAll(partsSearchLabel, partsSearchTextField);

        // add a key event handler to the search text field        
        partsSearchTextField.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
            @Override public void handle(KeyEvent event) {
                
                String partSearchString = partsSearchTextField.getText();
                
                // check if the pressed key is a-z, A-Z, or 0-9, or whitespace
                var alphanumericRegularExpression = "^[a-zA-Z0-9 ]*$";
                Pattern alphanumericPattern = Pattern.compile(alphanumericRegularExpression);
                Matcher alphanumericMatcher = alphanumericPattern.matcher(event.getCharacter());
                
                // append the current character to the previously typed characters if it matches the regex, otherwise ignore the new character
                if (alphanumericMatcher.matches()) {
                    partSearchString += event.getCharacter();
                }                
                
                // test the string with a regular expression, verify if it only contains digits 0-9 (Part ID)
                var digitRegularExpression = "\\d*";
                Pattern digitPattern = Pattern.compile(digitRegularExpression);
                Matcher digitMatcher = digitPattern.matcher(partSearchString);
                
                // if there is nothing typed, return all parts
                if (partSearchString.length() == 0) {
                    tableToUpdate.setItems(_inventory.getAllParts());
                    return;
                }
                
                // search Part ID or Part Name
                if (digitMatcher.matches()) { // part id search
                    int id = Integer.parseInt(partSearchString);
                    
                    Part p = _inventory.lookupPart(id);
                    ObservableList<Part> ol = FXCollections.observableArrayList();
                    if (p != null) {
                        ol.add(p);                        
                    }
                    
                    if(ol.size() == 0) {
                        var emptySearchAlert = new Alert(Alert.AlertType.ERROR, "No matching results!");
                        emptySearchAlert.show();
                    }
                    
                    tableToUpdate.setItems(ol);
                } else { // part name search
                    ObservableList<Part> ol = _inventory.lookupPart(partSearchString);
                    tableToUpdate.setItems(ol);
                    
                    if(ol.size() == 0) {
                        var emptySearchAlert = new Alert(Alert.AlertType.ERROR, "No matching results!");
                        emptySearchAlert.show();
                    }                    
                }
            }
        });
        
        return rootContainer;
    }

    /**
     * JavaFX GUI helper function for searching the products table
     * 
     * @param tableToUpdate TableView, table element to update rows when user inputs text to search
     * @return HBox, a horizontally organized container with GUI elements
     */    
    private HBox _getProductsSearchSection(TableView tableToUpdate) {
        var rootContainer = new HBox();
        rootContainer.setSpacing(10.0);
        rootContainer.setAlignment(Pos.TOP_RIGHT);

        var productSearchLabel = new Label("Search");
        var productSearchTextField = new TextField ();
        
        productSearchTextField.setPromptText("Product ID or Name");
        productSearchTextField.setPrefWidth(200);
        rootContainer.getChildren().addAll(productSearchLabel, productSearchTextField);

        // add a key event handler to the search text field        
        productSearchTextField.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
            @Override public void handle(KeyEvent event) {
                
                String productSearchString = productSearchTextField.getText();
                
                // check if the pressed key is a-z, A-Z, or 0-9, or whitespace
                var alphanumericRegularExpression = "^[a-zA-Z0-9 ]*$";
                Pattern alphanumericPattern = Pattern.compile(alphanumericRegularExpression);
                Matcher alphanumericMatcher = alphanumericPattern.matcher(event.getCharacter());
                
                // append the current character to the previously typed characters if it matches the regex, otherwise ignore the new character
                if (alphanumericMatcher.matches()) {
                    productSearchString += event.getCharacter();
                }                
                
                // test the string with a regular expression, verify if it only contains digits 0-9 (Product ID)
                var digitRegularExpression = "\\d*";
                Pattern digitPattern = Pattern.compile(digitRegularExpression);
                Matcher digitMatcher = digitPattern.matcher(productSearchString);
                
                // if there is nothing typed, return all products
                if (productSearchString.length() == 0) {
                    tableToUpdate.setItems(_inventory.getAllProducts());
                    return;
                }
                
                // search Product ID or Product Name
                if (digitMatcher.matches()) { // product id search
                    int id = Integer.parseInt(productSearchString);
                    
                    Product p = _inventory.lookupProduct(id);
                    ObservableList<Product> ol = FXCollections.observableArrayList();
                    if (p != null) {
                        ol.add(p);                
                    }
                    
                    if(ol.size() == 0) {
                        var emptySearchAlert = new Alert(Alert.AlertType.ERROR, "No matching results!");
                        emptySearchAlert.show();
                    }
                    
                    tableToUpdate.setItems(ol);
                } else { // product name search
                    ObservableList<Product> ol = _inventory.lookupProduct(productSearchString);
                    tableToUpdate.setItems(ol);
                    
                    if(ol.size() == 0) {
                        var emptySearchAlert = new Alert(Alert.AlertType.ERROR, "No matching results!");
                        emptySearchAlert.show();
                    }                    
                }
            }
        });
        
        return rootContainer;
    }

    /**
     * Creates a JavaFX element container and TableView for the inventory part list.
     * @return JavaFX VBox, container for organizing elements vertically
     */    
    private VBox _getPartsTableContainer() {
        var tableContainer = new VBox();

        this._partsTableView.setEditable(false);
        tableContainer.setPrefWidth(600);
        tableContainer.setPadding(new Insets(20, 0, 10, 0)); // top, right, bottom, and left padding
        tableContainer.getChildren().addAll(this._partsTableView);        
        
        
        var partIDTableColumn = new TableColumn("Part ID");
        partIDTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        partIDTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("id")
        );
        
        var partNameTableColumn = new TableColumn("Part Name");
        partNameTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 25); // set percentage of width
        partNameTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("name")
        );
        
        var inventoryLevelTableColumn = new TableColumn("Inventory Level");
        inventoryLevelTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 25); // set percentage of width
        inventoryLevelTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("stock")
        );
        
        var priceTableColumn = new TableColumn("Price / Cost per Unit");
        priceTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 35); // set percentage of width
        priceTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("price")
        );
        
        this._partsTableView.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        this._partsTableView.getColumns().addAll(partIDTableColumn, partNameTableColumn, inventoryLevelTableColumn, priceTableColumn);        
        this._partsTableView.setItems(_inventory.getAllParts());
        
        return tableContainer;
    }
    
    /**
     * Creates a JavaFX element container and TableView for the inventory product list.
     * @return JavaFX VBox, container for organizing elements vertically
     */
    private VBox _getProductsTableContainer() {
        var tableContainer = new VBox();

        this._productsTableView.setEditable(false);
        tableContainer.setPrefWidth(600);
        tableContainer.setPadding(new Insets(20, 0, 10, 0)); // top, right, bottom, and left padding
        tableContainer.getChildren().addAll(this._productsTableView);        
        
        
        var productIDTableColumn = new TableColumn("Product ID");
        productIDTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 20 ); // set percentage of width
        productIDTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("id")
        );
        
        var productNameTableColumn = new TableColumn("Product Name");
        productNameTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 20); // set percentage of width
        productNameTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("name")
        );
        
        var inventoryLevelTableColumn = new TableColumn("Inventory Level");
        inventoryLevelTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 25); // set percentage of width
        inventoryLevelTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("stock")
        );
        
        var priceTableColumn = new TableColumn("Price / Cost per Unit");
        priceTableColumn.setMaxWidth(1f * Integer.MAX_VALUE * 35); // set percentage of width
        priceTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Part,String>("price")
        );
        
        this._productsTableView.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        this._productsTableView.getColumns().addAll(productIDTableColumn, productNameTableColumn, inventoryLevelTableColumn, priceTableColumn);        
        this._productsTableView.setItems(_inventory.getAllProducts());
        
        return tableContainer;
    }
    
    
    /**
     * Entry to JavaFX GUI application.
     * @param stage top level JavaFX container.
     */
    @Override
    public void start(Stage stage) {
        var rootContainer = new BorderPane();
        var bodyContainer = new GridPane();
        rootContainer.setCenter(bodyContainer);
        
        // useful for debugging
        bodyContainer.setGridLinesVisible(false);
        bodyContainer.setPadding(new Insets(10));
        
        
        // get the parts and products form elements
        var partsPane = this._getMainFormPartsPane();
        var productsPane = this._getMainFormProductsPane();

        // set a top window menu
        var menuBar = this._getMainFormTopMenu();
        rootContainer.setTop(menuBar);            
        
        /*
            Organize the main form GUI components
        */
    
        var titleLabel = new Label ("Inventory Management System");
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 20.0)); 
        
        var scene = new Scene(rootContainer);
        bodyContainer.add (titleLabel, 1, 0);
        bodyContainer.add(partsPane, 1, 1);
        bodyContainer.add(productsPane, 2, 1);

        // exit button
        var buttonContainer = new HBox ();
        buttonContainer.setAlignment(Pos.TOP_RIGHT);
        var exitButton = new Button("Exit");
        buttonContainer.getChildren().add(exitButton);
        buttonContainer.setPadding(new Insets(10, 10, 0, 0)); // // top, right, bottom, left
        
        // exit button event handler
        exitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                System.exit (0);
            }
        });
        
        bodyContainer.add (buttonContainer, 2, 3);        
        
        // show the window
        stage.setScene(scene);
        stage.setTitle("WGU C482 - Software 01 - Performance Assessment");
        stage.show();  
    }

    /**
     * Javadoc files are in the folder: "[Top level of the zip file]]/wgu_c482_software_01_project/target/site/apidocs/index.html"
     * Used NetBeans Top Menu "Run->Generate Javadoc"
     * @param args string arguments passed to the application if launched from a terminal or command prompt
     */
    public static void main(String[] args) {
        launch();
    }

}
