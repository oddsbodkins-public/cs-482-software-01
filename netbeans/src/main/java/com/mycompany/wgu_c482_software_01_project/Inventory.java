/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c482_software_01_project;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.util.ArrayList;

/**
 * Class representing a business's physical inventory. Both Part units and Product units.
 * @author User
 */
public class Inventory {
    private ArrayList<Part> _partsList;
    private ArrayList<Product> _productsList;
    
    /**
     *
     */
    public Inventory() {
        this._partsList = new ArrayList<Part>();
        this._productsList = new ArrayList<Product>();
    }
    
    /**
     * 
     * @return list of all available part units. ObservableList class is used by the JavaFX TableView class to represent rows.
     */
    private ObservableList<Part> allParts() {
        ObservableList<Part> ol = FXCollections.observableArrayList(this._partsList);
        return ol;
    }
    
    /**
     * 
     * @return list of all available product units. ObservableList class is used by the JavaFX TableView class to represent rows.
     */
    private ObservableList<Product> allProducts() {
        ObservableList<Product> ol = FXCollections.observableArrayList(this._productsList);
        return ol;
    }
    
    /**
     * 
     * @param newPart part unit to add to business electronic inventory
     */
    public void addPart(Part newPart) {
        this._partsList.add(newPart);
    }
    
    /**
     * 
     * @param newProduct product unit to add to business electronic inventory
     */
    public void addProduct(Product newProduct) {
        this._productsList.add(newProduct);
    }

    
    /**
     *  Search the ArrayList from start to finish for a matching partID.
     *  Returns the first matching Part instance or null if not found.
     * 
     * @param partId part unit id to search for in inventory list
     * @return found part instance or null
     */
    public Part lookupPart(int partId) {
        for (int i = 0; i < this._partsList.size(); i++) {
            if(this._partsList.get(i).getId() == partId) {
                return this._partsList.get(i);
            }
        }
        
        return null;
    }
    

    /**
     * Search the ArrayList from start to finish for strings that contain the requested string.
     * Returns multiple instances of Part class.
     * 
     * RUNTIME ERROR
     * 
     * Fixed the run-time error of only matching case-sensitive names. Ex: 'Wheel' is different than 'wheel'.
     * Used the String.toLowerCase() method to make the name search case insensitive.
     * 
     * @param partName part unit name to search for in inventory list
     * @return all matching part instances
     */
    public ObservableList<Part> lookupPart(String partName) {
        ArrayList<Part> matching = new ArrayList<Part>();
        
        for (int i = 0; i < this._partsList.size(); i++) {
            if(this._partsList.get(i).getName().toLowerCase().contains(partName.toLowerCase())) {
                matching.add(this._partsList.get(i));
            }
        }
        
        ObservableList<Part> ol = FXCollections.observableArrayList(matching);
        return ol;
    }
    
    /**
     *  Search the ArrayList from start to finish for a matching productID.
     *  Returns the first matching Product instance or null if not found.
     * 
     * @param productId product unit id to search for in inventory list
     * @return found part instance or null
     */    
    public Product lookupProduct(int productId) {        
        for (int i = 0; i < this._productsList.size(); i++) {
            if (this._productsList.get(i).getId() == productId) {
                return this._productsList.get(i);
            }
        }
        
        return null;
    }
    
    /**
     * Search the ArrayList from start to finish for strings that contain the requested string.
     * Returns multiple instances of Product class.
     * 
     * RUNTIME ERROR
     * 
     * Fixed the run-time error of only matching case-sensitive names. Ex: 'Unicycle' is different than 'unicycle'.
     * Used the String.toLowerCase() method to make the name search case insensitive.
     * 
     * @param productName product unit name to search for in inventory list
     * @return all matching part instances
     */    
    public ObservableList<Product> lookupProduct(String productName) {
        ArrayList<Product> matching = new ArrayList<Product>();
        
        for (int i = 0; i < this._productsList.size(); i++) {
            if(this._productsList.get(i).getName().toLowerCase().contains(productName.toLowerCase())) {
                matching.add(this._productsList.get(i));
            }
        }
        
        ObservableList<Product> ol = FXCollections.observableArrayList(matching);
        return ol;
    }
    
    /**
     * Replace an existing Part instance with a new instance.
     * 
     * @param index the index in the internal ArrayList
     * @param selectedPart new instance of Part class to replace the existing one
     */
    public void updatePart(int index, Part selectedPart) {
        // verify that the index is valid
        if(index >= 0 && index < this._partsList.size()) {
            this._partsList.set(index, selectedPart);
        }
    }

    /**
     * Replace an existing Product instance with a new instance.
     * 
     * @param index the index in the internal ArrayList
     * @param newProduct new instance of Product class to replace the existing one
     */    
    public void updateProduct(int index, Product newProduct) {
        // verify that the index is valid
        if(index >= 0 && index < this._productsList.size()) {
            this._productsList.set(index, newProduct);
        }        
    }    

    /**
     * Remove an existing Part instance. Return true if deleted, false otherwise.
     * 
     * @param selectedPart instance of Part to remove from internal ArrayList
     * @return true if the part is removed from the inventory list.
     */
    public boolean deletePart(Part selectedPart) {
        return this._partsList.remove(selectedPart);
    }

    /**
     * Remove an existing Product instance. Return true if deleted, false otherwise.
     * 
     * @param selectedProduct instance of Product to remove from internal ArrayList
     * @return true if the product is removed from the inventory list.
     */    
    public boolean deleteProduct(Product selectedProduct) {
        return this._productsList.remove(selectedProduct);
    }
    
    /**
     * 
     * @return list of all available parts
     */
    public ObservableList<Part> getAllParts() {
        return this.allParts();
    }
    
    /**
     * 
     * @return list of all available products
     */
    public ObservableList<Product> getAllProducts() {
        return this.allProducts();
    }    
}
