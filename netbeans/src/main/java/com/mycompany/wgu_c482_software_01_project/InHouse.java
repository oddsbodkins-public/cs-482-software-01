/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c482_software_01_project;

/**
 * Child class of 'Part'. Adds one variable, an integer id for the machine producing the part in-house.
 * @author User
 */
public class InHouse extends Part {
    private int machineId;    
    
    /**
     * Class constructor, child of abstract class Part
     * @param id part id, inherited from Part class
     * @param name part name, inherited from Part class
     * @param price part price, inherited from Part class
     * @param stock part number of units in stock / inventory level, inherited from Part class
     * @param min minimum number of units required to be in stock, inherited from Part class
     * @param max maximum number of units allowed to be in stock, inherited from Part class
     * @param machineId, unique to InHouse, id of machine used to produce part
     */
    public InHouse(int id, String name, double price, int stock, int min, int max, int machineId) {
        super(id, name, price, stock, min, max);
        this.machineId = machineId;
    }
    
    /**
     * 
     * @param machineId the id of the machine producing the part in-house.
     */
    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }
    
    /**
     * 
     * @return the id of the machine producing the part in-house.
     */
    public int getMachineId() {
        return this.machineId;
    }
}
