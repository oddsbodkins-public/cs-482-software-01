/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c482_software_01_project;

/**
 * Child class of 'Part'. Adds one variable, a string representing the name of the company producing the outsourced part.
 * @author User
 */
public class Outsourced extends Part {
    private String companyName;
    
    /**
     * Class constructor, child of abstract class Part
     * @param id part id, inherited from Part class
     * @param name part name, inherited from Part class
     * @param price part price, inherited from Part class
     * @param stock part number of units in stock / inventory level, inherited from Part class
     * @param min minimum number of units required to be in stock, inherited from Part class
     * @param max maximum number of units allowed to be in stock, inherited from Part class
     * @param companyName unique to Outsourced class, name of the company producing the outsourced part
     */
    public Outsourced(int id, String name, double price, int stock, int min, int max, String companyName) {
        super(id, name, price, stock, min, max);
        this.companyName = companyName;
    }
    
    /**
     * 
     * @param companyName Set the name of the company producing the outsourced part.
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    /**
     * 
     * @return the name of the company producing the outsourced part.
     */
    public String getCompanyName() {
        return this.companyName;
    }
}
