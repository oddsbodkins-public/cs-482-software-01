/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c482_software_01_project;

/**
 * Helper class with static methods. Does not need to be instantiated.
 * @author User
 */
public class Utilities {
    /**
     * Determine if a user input string is composed of only digits 0-9 of any length.
     * Negative numbers and decimals (floating point numbers) are not allowed.
     * Negative numbers would not make sense for our use-case, there is no such thing as a negative inventory level.
     * @param string The user typed input, from the JavaFX TextField class.
     * @return true if the string matches expectations, otherwise false
     */
    public static boolean isValidPositiveInteger(String string) {
        // on an empty string, return false
        if (string.length() == 0 ) {
            return false;
        }        

        // test the string with a regular expression, verify if it only contains digits 0-9, no decimal, whitespaces, or negative allowed        
        return string.matches("\\d+");
    }
    
    /**
     * Determine if a user input string is composed of only digits with a single decimal point.
     * Negative numbers are not allowed, there is no such thing as a negative product price.
     * @param string The user typed input, from the JavaFX TextField class.
     * @return true if the string matches described requirements, otherwise false
     */
    public static boolean isValidPositiveDouble(String string) {
        // on an empty string, return false
        if (string.length() == 0 ) {
            return false;
        }
                
        if (string.matches("((\\d+\\.?\\d*)|(\\.\\d+))")) {
            double d = Double.parseDouble(string);
            if (d >= 0) {
                return true;
            }
        }
        
        return false;
    }    
}
