/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c482_software_01_project;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * Products represent an item to be sold.
 * A Product may require one or more 'Parts', represented by the abstract Part class.
 * This are called associated parts and may be added or removed as desired.
 * 
 * @author User
 */
public class Product {
    private ObservableList<Part> associatedParts;
    private int id;
    private String name;
    private double price;
    private int stock;
    private int min;
    private int max;
    
    /**
     *
     * @param id product id
     * @param name product name
     * @param price product price
     * @param stock number of units in stock / inventory level
     * @param min minimum number of units required to be in stock
     * @param max maximum number of units of units allowed to be in stock
     */
    public Product(int id, String name, double price, int stock, int min, int max) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.min = min;
        this.max = max;
        this.associatedParts = FXCollections.observableArrayList();
    }
    
    /*
        Setters
    */
    
    /**
     * @param id the product id to set
    */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * 
     * @param name the product name to set
     */
    public void setName(String name) {
        this.name = name;
        
    }
    
    /**
     * 
     * @param price the product price to set, assumed to be in USD currency. Ex: 1.50
     */
    public void setPrice(double price) {
        this.price = price;
    }
    
    /**
     * 
     * @param stock the product in stock or the number of units in inventory. Must be between min and max.
     */
    public void setStock(int stock) {
        this.stock = stock;
    }
    
    /**
     * 
     * @param min the minimum number of products required to be in stock. Must be less than max.
     */
    public void setMin(int min) {
        this.min = min;
    }
    
    /**
     * 
     * @param max the maximum number of products able to be stored in the inventory. Must be greater than min.
     */
    public void setMax(int max) {
        this.max = max;
    }

    /*
        Getters
    */
    
    /**
     * 
     * @return the product id
     */
    public int getId() {
        return this.id;
    }
    
    /**
     * 
     * @return the product name
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * 
     * @return the product price, assumed to be in USD currency. Ex: 1.50
     */
    public double getPrice() {
        return this.price;
    }
    
    /**
     * 
     * @return the number of product units in stock / the inventory level. Must be between min and max.
     */
    public int getStock() {
        return this.stock;
    }
    
    /**
     * 
     * @return the minimum number of product units required to be in stock. Must be less than max.
     */
    public int getMin() {
        return this.min;
    }
    
    /**
     * 
     * @return the maximum number of product units able to fit in stock. Must be greater than min.
     */
    public int getMax() {
        return this.max;
    }
    
    /*
        Other functions
    */
    
    /**
     * 
     * @param part One of the required parts for this product to be manufactured.
     */
    public void addAssociatedPart(Part part) {
        // do not add a part object if it is already in the list
        if(!this.associatedParts.contains(part)) {
            this.associatedParts.add(part);
        }
    }
    
    /**
     * 
     * @param selectedAssociatedPart The part to delete / unassociate from the current Product.
     * @return if the Part instance was successfully unassociated from the Product instance.
     */
    public boolean deleteAssociatedPart(Part selectedAssociatedPart) {
        return this.associatedParts.remove(selectedAssociatedPart);
    }
    
    /**
     * 
     * @return A list of all associated parts. 'ObservableList' is the class used by the JavaFX TableView class to represent its rows.
     */
    public ObservableList<Part> getAllAssociatedParts() {
        return this.associatedParts;
    }
    
}
